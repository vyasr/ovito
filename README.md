# OVITO - Open Visualization Tool

OVITO is a scientific data visualization and analysis software for atomistic, molecular and other
particle-based simulations. Visit the official website for more information and ready-to-use download packages:

<https://www.ovito.org/>

The user documentation for the latest release of OVITO is found here:

<https://www.ovito.org/docs/current/>

Note that the software vendor OVITO GmbH offers two official versions of the program: OVITO Basic and OVITO Pro.
OVITO Basic, which provides only a subset of the functions of OVITO Pro, is based on the source code in this 
repository. 

## Building

Build instructions for the source code can be found here:

<https://www.ovito.org/docs/current/development.php>

Note that the resulting application will not have the branding "OVITO Basic".
It will identify itself as just "OVITO", but it provides roughly the same functionality as the 
official OVITO Basic distributed by the vendor OVITO GmbH.

## Contributing

OVITO is an open source project and we are very happy to accept community contributions.
Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.
